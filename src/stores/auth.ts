import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import router from "@/router";
export const useAuthStore = defineStore("auth", () => {
  const authName = ref("");
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const isauth = computed(() => {
    // authName is not empty
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  });
  const login = async (userName: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(userName, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      console.log("Success");
      router.push("/"); //path
    } catch (error) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;

    // authName.value = userName;
    //  localStorage.setItem("authName", userName);
  };
  const logout = () => {
    // authName.value = "";
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };
  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };
  return { login, logout, isauth, isLogin };
});
